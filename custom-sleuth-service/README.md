## Example Spring Cloud sleuth application with custom configuration
This example describes how to propagate headers to Kafka using custom propagation mechanism.
To create custom propagation mechanism need to implement your own bean `Propagation.Factory` for Brave

**Steps:**

1. Create new project
2. Add the necessary dependencies to the **`build.gradle`**:

   `implementation 'org.springframework.boot:spring-boot-starter-web'`

   `implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'`

   `implementation 'org.springframework.kafka:spring-kafka'`

3. Create controller, kafkaProducer, kafkaConsumer and implement Propagation.Factory(example com.krasko.sleuth.propagation.CustomRemoteKeysPropagator)
4. Start docker containers: `docker-compose up -d`
5. Run CustomSleuthServiceApplication 
6. Send http request `POST /api/sleuth/kafka` with custom headers `header-1`.

The response will be: `success`

Logs should contain the `traceId` and `header-1`

If you check the logs you should see a similar output:

`2021-12-20 16:07:48.144  INFO [Custom-Sleuth-Service,1da5a4608a11b6fc,1da5a4608a11b6fc] 111767 --- [nio-8085-exec-8] com.krasko.sleuth.kafka.KafkaProducer    : Message was successfully sent to the topic test-sleuth-kafka-topic`

`2021-12-20 16:07:48.147  INFO [Custom-Sleuth-Service,55447ca7a297b2a3,55447ca7a297b2a3] 111767 --- [ntainer#0-0-C-1] com.krasko.sleuth.kafka.KafkaConsumer    : Received message: Message(message=Hello world!)`

`2021-12-20 16:07:48.148  INFO [Custom-Sleuth-Service,55447ca7a297b2a3,55447ca7a297b2a3] 111767 --- [ntainer#0-0-C-1] com.krasko.sleuth.kafka.KafkaConsumer    : Received headers: {kafka_offset=11, header-1=1, kafka_consumer=brave.kafka.clients.TracingConsumer@3b3fff0, kafka_timestampType=CREATE_TIME, kafka_receivedPartitionId=0, kafka_receivedTopic=test-sleuth-kafka-topic, kafka_receivedTimestamp=1640009268144, kafka_groupId=sleuth-group}`

---
## Debugging
**1. Creates a new span based on parameters extracted from an incoming request.**

New span creating in the class `Tracer` method `nextSpan`(brave.Tracer.nextSpan(brave.propagation.TraceContextOrSamplingFlags))
We can set break point in this method and debugging process creating new span

**2. Extract and Inject baggage**

To propagation Spring Cloud Sleuth using interface `Propagation`(brave.propagation.Propagation).
To propagation baggage using class `CustomRemoteKeysPropagator`(com.krasko.sleuth.propagation.CustomRemoteKeysPropagator) which implement interface `Propagation`.
To debugging baggage we should use methods `inject`(com.krasko.sleuth.propagation.CustomRemoteKeysPropagator.inject) and `extract`(com.krasko.sleuth.propagation.CustomRemoteKeysPropagator.extract)

* `extract`(com.krasko.sleuth.propagation.CustomRemoteKeysPropagator.extract) - extract baggage from input request and set to the trace context

* `inject`(com.krasko.sleuth.propagation.CustomRemoteKeysPropagator.inject) - get baggage from trace context and set to the output request
package com.krasko.sleuth.kafka;

import com.krasko.sleuth.kafka.dto.Message;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaProducer {

  @Value("${app.kafka.topics.sleuth-kafka.name}")
  private String sleuthKafkaTopicName;
  private final KafkaTemplate<String, Message> kafkaTemplate;

  public KafkaProducer(
      KafkaTemplate<String, Message> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  public void sendMessageToSleuthKafkaTopic(Message message) {
    kafkaTemplate.send(sleuthKafkaTopicName, message);
    log.info("Message was successfully sent to the topic {}", sleuthKafkaTopicName);
  }
}

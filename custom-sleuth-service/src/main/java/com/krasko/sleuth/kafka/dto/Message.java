package com.krasko.sleuth.kafka.dto;

import lombok.Data;

@Data
public class Message {

  private String message;
}

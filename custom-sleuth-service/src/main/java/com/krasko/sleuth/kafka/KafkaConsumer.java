package com.krasko.sleuth.kafka;

import com.krasko.sleuth.kafka.dto.Message;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaConsumer {

  // Listen topic test-sleuth-kafka-feign-topic
  @KafkaListener(topics = "${app.kafka.topics.sleuth-kafka.name}")
  @KafkaHandler
  public void handler(@Payload Message message, @Headers Map<String, Object> headers) {
    log.info("Received message: {}", message);
    log.info("Received headers: {}", headers);
  }
}

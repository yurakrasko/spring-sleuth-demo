package com.krasko.sleuth.propagation;

import brave.baggage.BaggageField;
import brave.internal.baggage.BaggageFields;
import brave.internal.propagation.StringPropagationAdapter;
import brave.propagation.Propagation;
import brave.propagation.SamplingFlags;
import brave.propagation.TraceContext;
import brave.propagation.TraceContextOrSamplingFlags;
import java.util.Arrays;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class CustomRemoteKeysPropagator extends Propagation.Factory implements Propagation<String> {

  String customHeaderName = "header-1";
  String defaultCustomHeaderValue = "testValue";

  private final BaggageFields.Factory baggageFactory = BaggageFields.newFactory(List.of(), 5);

  @Override
  public List<String> keys() {
    return Arrays.asList(customHeaderName);
  }

  @Override
  public <R> TraceContext.Injector<R> injector(Setter<R, String> setter) {
    return (traceContext, request) -> {
      var baggageFields = traceContext.findExtra(BaggageFields.class);
      if (baggageFields == null) {
        return;
      }
      List<BaggageField> allFields = baggageFields.getAllFields();
      for (BaggageField field : allFields){
        String value = field.getValue(traceContext);
        setter.put(request, field.name(), value);
      }
    };
  }

  @Override
  public <R> TraceContext.Extractor<R> extractor(Getter<R, String> getter) {
    return request -> {
      if (request instanceof Boolean) {
        return TraceContextOrSamplingFlags.EMPTY;
      }
      var builder =
          TraceContextOrSamplingFlags.newBuilder(SamplingFlags.NOT_SAMPLED)
              .sampledLocal();
      BaggageFields extra = baggageFactory.create();
      String headerValue = getter.get(request, customHeaderName);
      if (headerValue == null){
        headerValue = defaultCustomHeaderValue;
      }
      extra.updateValue(BaggageField.create(customHeaderName), headerValue);
      builder.addExtra(extra);
      return builder.build();
    };
  }

  @Override
  public <K> Propagation<K> create(KeyFactory<K> keyFactory) {
    return StringPropagationAdapter.create(this, keyFactory);
  }

}

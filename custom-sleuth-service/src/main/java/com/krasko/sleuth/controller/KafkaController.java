package com.krasko.sleuth.controller;

import com.krasko.sleuth.kafka.KafkaProducer;
import com.krasko.sleuth.kafka.dto.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/sleuth/kafka")
@RequiredArgsConstructor
public class KafkaController {

  private final KafkaProducer producer;

  @PostMapping
  public String sendToSleuthKafkaTopic(@RequestBody Message message) {
    producer.sendMessageToSleuthKafkaTopic(message);
    return "success";
  }
}

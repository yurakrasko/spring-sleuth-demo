package com.krasko.dummy.controller;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/dummy/service")
public class DummyController {

  @GetMapping
  public String helloDummy(@RequestHeader Map<String, String> headers) {
    log.info("Dymmy service receive headers: {}", headers);
    var message = "Hello, I am Dummy service";
    log.info(message);
    return message;
  }
}

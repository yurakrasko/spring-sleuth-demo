package com.krasko.sleuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class DefaultSleuthServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(DefaultSleuthServiceApplication.class, args);
  }

}

package com.krasko.sleuth.example1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/sleuth")
public class SleuthController {

  @GetMapping
  public String helloSleuth() {
    var message = "Hello Sleuth";
    log.info(message);
    return message;
  }
}

package com.krasko.sleuth.example2.kafka;

import com.krasko.sleuth.example2.kafka.dto.Message;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaProducer {

  @Value("${app.kafka.topics.sleuth-kafka.name}")
  private String sleuthKafkaTopicName;
  @Value("${app.kafka.topics.sleuth-kafka-feign.name}")
  private String sleuthKafkaFeignTopicName;
  private final KafkaTemplate<String, Message> kafkaTemplate;

  public KafkaProducer(
      KafkaTemplate<String, Message> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  public void sendMessageToSleuthKafkaTopic(Message message){
    log.info("MDC context: {}", MDC.getCopyOfContextMap());
    kafkaTemplate.send(sleuthKafkaTopicName, message);
    log.info("Message was successfully sent to the topic {}", sleuthKafkaTopicName);
  }

  public void sendMessageToSleuthKafkaFeignTopic(Message message){
    log.info("MDC context: {}", MDC.getCopyOfContextMap());
    kafkaTemplate.send(sleuthKafkaFeignTopicName, message);
    log.info("Message was successfully sent to the topic {}", sleuthKafkaFeignTopicName);
  }
}

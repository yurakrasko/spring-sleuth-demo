package com.krasko.sleuth.example2;

import com.krasko.sleuth.example2.kafka.KafkaProducer;
import com.krasko.sleuth.example2.kafka.dto.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/sleuth/kafka")
@RequiredArgsConstructor
public class KafkaController {

  private final KafkaProducer producer;

  @PostMapping
  public String sendToSleuthKafkaTopic(@RequestBody Message message) {
    producer.sendMessageToSleuthKafkaTopic(message);
    return "success";
  }

  @PostMapping("/feign")
  public String sendToSleuthKafkaFeignTopic(@RequestBody Message message) {
    producer.sendMessageToSleuthKafkaFeignTopic(message);
    return "success";
  }
}

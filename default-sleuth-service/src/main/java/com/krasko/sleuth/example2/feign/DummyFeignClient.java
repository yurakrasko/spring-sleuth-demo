package com.krasko.sleuth.example2.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "DummyService", url = "http://localhost:8089")
public interface DummyFeignClient {

  @GetMapping("/api/dummy/service")
  String getHelloDummy();
}

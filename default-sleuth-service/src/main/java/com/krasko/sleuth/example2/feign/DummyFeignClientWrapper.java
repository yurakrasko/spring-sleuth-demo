package com.krasko.sleuth.example2.feign;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DummyFeignClientWrapper {

  private final DummyFeignClient feignClient;

  public String getHelloDummy() {
    return feignClient.getHelloDummy();
  }
}

package com.krasko.sleuth.example2.kafka;

import com.krasko.sleuth.example2.feign.DummyFeignClientWrapper;
import com.krasko.sleuth.example2.kafka.dto.Message;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaConsumer {

  private final DummyFeignClientWrapper feignClientWrapper;

  // Listen topic test-sleuth-kafka-topic
  @KafkaListener(topics = "${app.kafka.topics.sleuth-kafka.name}")
  @KafkaHandler
  public void messageHandler(@Payload Message message, @Headers Map<String, String> headers) {
    log.info("Received message: {}", message);
    log.info("Received headers: {}", headers);
  }

  // Listen topic test-sleuth-kafka-feign-topic
  @KafkaListener(topics = "${app.kafka.topics.sleuth-kafka-feign.name}")
  @KafkaHandler
  public void handler(@Payload Message message, @Headers Map<String, String> headers) {
    log.info("Received message: {}", message);
    log.info("Received headers: {}", headers);
    var dummyServiceResponse = feignClientWrapper.getHelloDummy();
    log.info("Dummy service response: {}", dummyServiceResponse);
  }
}

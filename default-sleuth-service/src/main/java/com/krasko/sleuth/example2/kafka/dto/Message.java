package com.krasko.sleuth.example2.kafka.dto;

import lombok.Data;

@Data
public class Message {

  private String message;
}

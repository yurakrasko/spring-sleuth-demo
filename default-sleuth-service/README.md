## Example Spring Cloud Sleuth application with default configuration
Current example contains two examples:

1. Example1 - how add Spring Cloud Sleuth to the project
2. Example2 - how propagate custom headers to FeignClient, Kafka and MDC context using default configuration

---
### Example1 - how add Spring Cloud Sleuth to the project
This example describes how to develop a small “Hello Sleuth!” web application that highlights some of Spring Cloud Sleuth’s key features.

**Steps:**

1. Create new project
2. Add the necessary dependencies to the **`build.gradle`**:

   `implementation 'org.springframework.boot:spring-boot-starter-web'`

   `implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'`

3. Create controller(example SleuthController)
4. Run DefaultSleuthServiceApplication
5. Send http request `GET /api/sleuth`

The response will be: `Hello Sleuth`
If you check the logs you should see a similar output:
`2021-12-20 13:42:24.388  INFO [Sleuth-Service,eb79c1bd638cd174,eb79c1bd638cd174] 74027 --- [nio-8083-exec-3] c.k.sleuth.example1.SleuthController     : Hello Sleuth
`

`[Sleuth-Service,eb79c1bd638cd174,eb79c1bd638cd174]` - this entry corresponds to `[application name,trace id, span id]`


### Example2 - how propagate custom headers to FeignClient and Kafka using default configuration
This example describes how to propagate custom headers to FeignClient, Kafka and MDC context

**Steps:**

1. Create new project
2. Add the necessary dependencies to the **`build.gradle`**:

   `implementation 'org.springframework.boot:spring-boot-starter-web'`

   `implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'`

   `implementation 'org.springframework.kafka:spring-kafka'`

   `implementation 'org.springframework.cloud:spring-cloud-starter-openfeign'`

3. Create controller, feignClient, kafkaProducer and kafkaConsumer
4. Start docker containers: `docker-compose up -d`
5. Run DummyServiceApplication
6. Run DefaultSleuthServiceApplication
7. Send http request `POST /api/sleuth/kafka` with custom headers `header-1, header-2`.
In this flow will be generated traceId/spanId and propagate to the Kafka and MDC context custom headers `header-1, header-2`
8. Send http request `POST /api/sleuth/kafka/feign` with custom headers `header-1, header-2`.
In this flow will be generated traceId/spanId and propagate to the FeignClient, Kafka and MDC context custom headers `header-1, header-2`

The response will be: `success`

Logs should contain the `traceId` and `header-1, header-2`

If you check the logs you should see a similar output(for request `POST /api/sleuth/kafka`):
`2021-12-20 13:56:04.220  INFO [Sleuth-Service,354c1946d841aeb5,354c1946d841aeb5] 76738 --- [nio-8083-exec-5] c.k.sleuth.example2.kafka.KafkaProducer  : Message was successfully sent to the topic test-sleuth-kafka-topic`

`2021-12-20 13:56:04.224  INFO [Sleuth-Service,354c1946d841aeb5,6b20371d8b5cef94] 76738 --- [ntainer#1-0-C-1] c.k.sleuth.example2.kafka.KafkaConsumer  : Received message: Message(message=Hello world!)`

`2021-12-20 13:56:04.224  INFO [Sleuth-Service,354c1946d841aeb5,6b20371d8b5cef94] 76738 --- [ntainer#1-0-C-1] c.k.sleuth.example2.kafka.KafkaConsumer  : Received headers: {kafka_offset=3, header-2=testHeaderValue2, header-1=testHeaderValue1, kafka_consumer=brave.kafka.clients.TracingConsumer@3ed809c, kafka_timestampType=CREATE_TIME, kafka_receivedPartitionId=0, kafka_receivedTopic=test-sleuth-kafka-topic, kafka_receivedTimestamp=1640001364220, kafka_groupId=sleuth-group}`


If you check the logs you should see a similar output(for request `POST /api/sleuth/kafka/feign`):
`2021-12-20 14:03:53.997  INFO [Sleuth-Service,f1eab9c06c8fe90f,f1eab9c06c8fe90f] 79672 --- [nio-8083-exec-5] c.k.sleuth.example2.kafka.KafkaProducer  : MDC context: {traceId=f1eab9c06c8fe90f, spanId=f1eab9c06c8fe90f, header-2=2, header-1=1}`

`2021-12-20 14:03:53.997  INFO [Sleuth-Service,f1eab9c06c8fe90f,f1eab9c06c8fe90f] 79672 --- [nio-8083-exec-5] c.k.sleuth.example2.kafka.KafkaProducer  : Message was successfully sent to the topic test-sleuth-kafka-feign-topic`

`2021-12-20 14:03:54.002  INFO [Sleuth-Service,f1eab9c06c8fe90f,9f03017aa7b0eabc] 79672 --- [ntainer#0-0-C-1] c.k.sleuth.example2.kafka.KafkaConsumer  : Received message: Message(message=Hello world!)`

`2021-12-20 14:03:54.002  INFO [Sleuth-Service,f1eab9c06c8fe90f,9f03017aa7b0eabc] 79672 --- [ntainer#0-0-C-1] c.k.sleuth.example2.kafka.KafkaConsumer  : Received headers: {kafka_offset=5, header-2=2, header-1=1, kafka_consumer=brave.kafka.clients.TracingConsumer@69b93b49, kafka_timestampType=CREATE_TIME, kafka_receivedPartitionId=0, kafka_receivedTopic=test-sleuth-kafka-feign-topic, kafka_receivedTimestamp=1640001833997, kafka_groupId=sleuth-group}'`

`2021-12-20 14:03:54.016  INFO [Sleuth-Service,f1eab9c06c8fe90f,9f03017aa7b0eabc] 79672 --- [ntainer#0-0-C-1] c.k.sleuth.example2.kafka.KafkaConsumer  : Dummy service response: Hello, I am Dummy service`

Dummy service logs:
`2021-12-20 14:03:54.006  INFO [Dummy-Service,f1eab9c06c8fe90f,13316ea0bbd9e9bc] 79497 --- [nio-8089-exec-2] c.k.dummy.controller.DummyController     : Dymmy service receive headers: {x-b3-traceid=f1eab9c06c8fe90f, x-b3-spanid=13316ea0bbd9e9bc, x-b3-parentspanid=9f03017aa7b0eabc, x-b3-sampled=0, header-1=1, header-2=2, accept=*/*, user-agent=Java/11.0.11, host=localhost:8089, connection=keep-alive}`

`2021-12-20 14:03:54.006  INFO [Dummy-Service,f1eab9c06c8fe90f,13316ea0bbd9e9bc] 79497 --- [nio-8089-exec-2] c.k.dummy.controller.DummyController     : Hello, I am Dummy service`

---
## Debugging
**1. Creates a new span based on parameters extracted from an incoming request.**

New span creating in the class `Tracer` method `nextSpan`(brave.Tracer.nextSpan(brave.propagation.TraceContextOrSamplingFlags))
We can set break point in this method and debugging process creating new span

**2. Extract and Inject baggage**

To propagation Spring Cloud Sleuth using interface `Propagation`(brave.propagation.Propagation). 
To propagation baggage using class `BaggagePropagation`(brave.baggage.BaggagePropagation) which implement interface `Propagation`.
To debugging baggage we should use methods `inject`(brave.baggage.BaggagePropagation.BaggageInjector.inject) and `extract`(brave.baggage.BaggagePropagation.BaggageExtractor.extract)

* `extract`(brave.baggage.BaggagePropagation.BaggageExtractor.extract) - extract baggage from input request and set to the trace context

* `inject`(brave.baggage.BaggagePropagation.BaggageInjector.inject) - get baggage from trace context and set to the output request

By default Spring Cloud Sleuth using propagation type `B3`. To extract/inject `B3` headers using `B3Propagation` class
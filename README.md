# Spring Cloud Sleuth
Spring-cloud-sleuth is used to trace the request propagation within the micro-services. It is used along with the logs to trace the request. In this we create trace-id and span-id that is appended with log which is used in the debugging of the flow.
[More details](https://docs.spring.io/spring-cloud-sleuth/docs/current-SNAPSHOT/reference/html/getting-started.html#getting-started-introducing-spring-cloud-sleuth)

---
## Main concepts
The main elements in the Spring Cloud Sleuth are Span and Trace

**Span** — the basic unit of work. For every process a new span-id is created.

**Trace** — a set of spans forming a tree-like structure. trace-id is an id that is assigned to a single request, job, or particular thread of action.

### How is Spring Cloud Sleuth work
Spring Cloud Sleuth does is it creates a trace-id which is unique and common for the request flow. Another span-id is created which is different for each unit of work. So while Logging, we append these span-id and trace-id to the log statements. Also in case of micro-services, we pass these ids and our custom key(if required) in each hop from micro-service to micro-service.
Now that every log statements have these trace-id(unique and common for the flow) and span-id. We grep the Logs for this trace-id and then we trace the particular request flow. This in turns makes debugging easier for developers using logs.

The following image shows how Span and Trace look in a system.

<p align="center">
  <img width="800" height="500" src="./trace-span.png">
</p>


Notice how in the diagram above, the trace is the same throughout the entire processing flow. A trace will tie a tree of branching processing functions and server requests into a single, trackable unit. Also notice how each request to a new service automatically starts a new span. However, there is also a custom function. In this case, the process has started a side-process and has manually created and terminated a span.

In practice, the span and trace IDs look like the following in log entries **`INFO [service1,2485ec27856c56f4,2485ec27856c56f4]`**.
This entry corresponds to **`[application name,trace id, span id]`**. The application name got read from the **`SPRING_APPLICATION_NAME`** environment variable.
[More details](https://docs.spring.io/spring-cloud-sleuth/docs/current-SNAPSHOT/reference/html/getting-started.html#getting-started-terminology)

---
## Spring Cloud Sleuth Integrations
Spring Cloud Sleuth has integration with:

* HTTP Client;
* HTTP Server;
* Messaging(Kafka, RabbitMq, Kafka Streams, JMS);
* OpenFeign;
* OpenTracing;
* Quartz;
* Reactor;
* Redis;
* RPC;
* RxJava;
* Spring(Cloud CircuitBreaker, Cloud Config Server, Cloud Deployer, Cloud RSocket, Batch, Cloud Task, Security, JDBC, MongoDB, Session);

This all integration enabled by default, but it can disable in the setting([see](https://docs.spring.io/spring-cloud-sleuth/docs/current-SNAPSHOT/reference/html/integrations.html#sleuth-integration))

Example disable tracing for Apache Kafka: **`spring.sleuth.kafka.enabled: false`**

---
## Spring Cloud Sleuth Features
Spring Cloud Sleuth has features that you may want to use and customize([see](https://docs.spring.io/spring-cloud-sleuth/docs/current-SNAPSHOT/reference/html/project-features.html#features)).
    
**1. Context Propagation**

Traces connect from service to service using header propagation. By default Spring Cloud Sleuth using B3 format.

Spring Cloud Sleuth support propagation types:

* B3([see](https://github.com/openzipkin/b3-propagation))
* AWS
* W3C
* CUSTOM([see](https://docs.spring.io/spring-cloud-sleuth/docs/current-SNAPSHOT/reference/html/howto.html#how-to-change-context-propagation))

To use the provided defaults you can set the **`spring.sleuth.propagation.type`** property. The value can be a list in which case you will propagate more tracing headers.

**2. Sampling**

Spring Cloud Sleuth pushes the sampling decision down to the tracer implementation. However, there are cases where you can change the sampling decision at runtime.

One of such cases is skip reporting of certain client spans. To achieve that you can set the **`spring.sleuth.web.client.skip-pattern`** with the path patterns to be skipped. 

Another option is to provide your own custom **``org.springframework.cloud.sleuth.SamplerFunction<`org.springframework.cloud.sleuth.http.HttpRequest>``** implementation and define when a given HttpRequest should not be sampled.

**3. Baggage**

Distributed tracing works by propagating fields inside and across services that connect the trace together: traceId and spanId notably. The context that holds these fields can optionally push other fields that need to be consistent regardless of many services are touched. The simple name for these extra fields is "Baggage".

Sleuth allows you to define which baggage are permitted to exist in the trace context, including what header names are used.

You can use properties to define fields that have no special configuration such as name mapping:

* **`spring.sleuth.baggage.remote-fields`**  - is a list of header names to accept and propagate to remote services.
* **`spring.sleuth.baggage.local-fields`**  - is a list of names to propagate locally.
* **`spring.sleuth.baggage.correlation-fields`**  - is a list of fields(remote and local) that should be propagated to Slf4j’s MDC context.
* **`spring.sleuth.baggage.tag-fields`**  - is a list of fields that should automatically become tags.

**4. OpenZipkin Brave Tracer Integration**
Spring Cloud Sleuth integrates with the OpenZipkin Brave tracer via the bridge that is available in the spring-cloud-sleuth-brave module. In this section you can read about specific Brave integrations.
More details([see](https://docs.spring.io/spring-cloud-sleuth/docs/current-SNAPSHOT/reference/html/project-features.html#features-brave))

**5. Log integration**

Sleuth configures the logging context with variables including the service name **`{spring.application.name}`** if the previous one was not set), span ID (**`{spanId}`**) and the trace ID (**`{traceId}`**). These help you connect logs with distributed traces and allow you choice in what tools you use to troubleshoot your services.
But we can configure own format of logging. You can disable it by disabling Sleuth via **`spring.sleuth.enabled=false`** property or putting your own **`logging.pattern.level`** property.

---
## Spring Cloud Sleuth configuration

Various properties can be specified inside your **`application.properties`** file, inside your **`application.yml file`**, or as command line switches. A list of common Spring Cloud Sleuth properties and references to the underlying classes that consume them.
[Link to all configuration properties](https://docs.spring.io/spring-cloud-sleuth/docs/current-SNAPSHOT/reference/html/appendix.html#appendix)

---
## Examples
**[Example Spring Cloud sleuth application with default configuration](./default-sleuth-service)**

**[Example Spring Cloud sleuth application with custom configuration](./custom-sleuth-service)**